Rails.application.routes.draw do
  get 'locations/index'
  scope '/api' do
    resources :degrees, only: [:index] do
      resources :programmes, only: [:index]
    end
    resources :programmes, only: [:index]
    get 'contract_types' => "contract_types#index"
    get 'locations' => "locations#index"
  end

  resources :market_favorites
  get 'blat' => 'blat#index', as: 'blat'

  resources :applications, only: [:create, :show, :destroy]

  scope '/admin' do
    resources :job_offers, path: 'ofertas', except: [:new] do
      member do
        patch 'send_applications' => 'job_offers#send_applications', as: :send_applications
        patch 'trash' => 'job_offers#trash', as: :trash
        patch 'restore' => 'job_offers#restore', as: :restore
        patch 'approve' => 'job_offers#approve', as: :approve
      end
    end
    resources :settings
    resources :site_assets
    resources :markets, path: 'feira', except: [:show, :update, :destroy]
    resources :degrees, path: 'graus'
    resources :programmes, path: 'cursos'
    get 'empresas/qr' => "companies#qr_codes", as: :companies_qr
    resources :companies, path: 'empresas', except: [:show, :update, :destroy] do
      collection do
        get 'new/:nif' => 'companies#nif_lookup', as: :nif_lookup
      end
    end
  end
  resources :companies, path: 'empresa', only: [:show, :update, :destroy]
  resources :markets, param: :market_id, path: 'feira', only: [:show, :update, :destroy] do
    member do
      get 'empresa/:id' => 'companies#show', as: :company
    end
  end
  resources :job_offers, path: '/oferta', only: [:new] do
    collection do
      get 'estudantes/:random_id' => 'job_offers#students', as: :students
    end
  end

  # devise_for :users
  get 'users/codes' => "users#random_index"
  devise_for :users, path: '', path_names: { sign_in: 'entrar', sign_out: 'sair'}
  resources :users do
    collection do
      resources :profiles
    end
  end
  match 'admin' => 'degrees#index', via: :get

  patch 'toggle_review/:id' => 'profiles#toggle_review', as: :profile_review
  patch 'profiles/:id/incomplete' => 'profiles#incomplete_profile', as: :incomplete_profile
  delete 'delete_curriculum/:id' => 'profiles#delete_curriculum', as: :delete_curriculum

  match 'perfil' => 'profiles#show', as: :my_profile, via: :get
  match 'perfil/editar' => 'profiles#edit', as: :edit_my_profile, via: :get
  match 'utilizador' => 'users#show', as: :my, via: :get
  get 'utilizador/qr' => 'users#qr_code', as: :my_qr

  get 'feira' => 'pages#market', as: :market_page
  scope 'feira', as: 'market' do
    resources :companies, only: [:show], path: :empresa do
      member do
        get 'qr' => "companies#company_qr", as: :qr
      end
    end
  end
  root 'pages#index'
end
