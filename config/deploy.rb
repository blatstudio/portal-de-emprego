# config valid only for current version of Capistrano
lock "3.11.0"

set :application, "emprego"
set :repo_url, "git@bitbucket.org:blatstudio/portal-de-emprego.git"

# Server
set :deploy_user, "rails"
set :use_sudo, false
# set :rails_env, "production"
set :deploy_via, :copy
set :ssh_options, { port: 1337 }
server "blatstudio.com"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :branch, ENV.fetch("BRANCH") {"master"}

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/#{fetch(:deploy_user)}/#{fetch(:application)}#{fetch(:branch) != 'master' ? '-' + fetch(:branch) : '' }"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
append :linked_files, "config/application.yml", "Passengerfile.json"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "vendor/bundle"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :passenger_restart_with_touch, true

# Setup rbenv.
set :rbenv_type, :user
set :rbenv_ruby, '2.5.1'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}

set :bundle_flags, '--deployment'

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:branch)}" }
