set :stage, :production
set :branch, ENV.fetch("BRANCH") {"master"}

set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"
set :server_name, "blatstudio.com"
server 'blatstudio.com', user: 'rails', roles: %w{app db web}, my_property: true
set :rails_env, :production
