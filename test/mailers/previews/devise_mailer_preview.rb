# Preview all emails at http://localhost:3000/rails/mailers/devise_mailer
class DeviseMailerPreview < ActionMailer::Preview

  def invitation_instructions
    user = User.with_role(:recruiter).last
    Devise::Mailer.invitation_instructions(user, user.confirmation_token)
  end

  def confirmation_instructions
    Devise::Mailer.confirmation_instructions(User.last, User.last.confirmation_token)
  end

end
