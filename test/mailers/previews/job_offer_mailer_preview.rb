# Preview all emails at http://localhost:3000/rails/mailers/job_offer_mailer
class JobOfferMailerPreview < ActionMailer::Preview

  def new_job_offer
    JobOfferMailer.new_job_offer(JobOffer.last)
  end

  def job_offer_expired
    JobOfferMailer.job_offer_expired(JobOffer.last)
  end

  def job_offer_approved
    JobOfferMailer.job_offer_approved(JobOffer.last)
  end

  def job_offer_reproved
    JobOfferMailer.job_offer_reproved(JobOffer.last)
  end

  def applications_link
    JobOfferMailer.applications_link(JobOffer.last)
  end

end
