require "application_system_test_case"

class MarketFavoritesTest < ApplicationSystemTestCase
  setup do
    @market_favorite = market_favorites(:one)
  end

  test "visiting the index" do
    visit market_favorites_url
    assert_selector "h1", text: "Market Favorites"
  end

  test "creating a Market favorite" do
    visit market_favorites_url
    click_on "New Market Favorite"

    fill_in "Company", with: @market_favorite.company_id
    fill_in "Market", with: @market_favorite.market_id
    fill_in "Profile", with: @market_favorite.profile_id
    click_on "Create Market favorite"

    assert_text "Market favorite was successfully created"
    click_on "Back"
  end

  test "updating a Market favorite" do
    visit market_favorites_url
    click_on "Edit", match: :first

    fill_in "Company", with: @market_favorite.company_id
    fill_in "Market", with: @market_favorite.market_id
    fill_in "Profile", with: @market_favorite.profile_id
    click_on "Update Market favorite"

    assert_text "Market favorite was successfully updated"
    click_on "Back"
  end

  test "destroying a Market favorite" do
    visit market_favorites_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Market favorite was successfully destroyed"
  end
end
