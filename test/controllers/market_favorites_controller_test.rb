require 'test_helper'

class MarketFavoritesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @market_favorite = market_favorites(:one)
  end

  test "should get index" do
    get market_favorites_url
    assert_response :success
  end

  test "should get new" do
    get new_market_favorite_url
    assert_response :success
  end

  test "should create market_favorite" do
    assert_difference('MarketFavorite.count') do
      post market_favorites_url, params: { market_favorite: { company_id: @market_favorite.company_id, market_id: @market_favorite.market_id, profile_id: @market_favorite.profile_id } }
    end

    assert_redirected_to market_favorite_url(MarketFavorite.last)
  end

  test "should show market_favorite" do
    get market_favorite_url(@market_favorite)
    assert_response :success
  end

  test "should get edit" do
    get edit_market_favorite_url(@market_favorite)
    assert_response :success
  end

  test "should update market_favorite" do
    patch market_favorite_url(@market_favorite), params: { market_favorite: { company_id: @market_favorite.company_id, market_id: @market_favorite.market_id, profile_id: @market_favorite.profile_id } }
    assert_redirected_to market_favorite_url(@market_favorite)
  end

  test "should destroy market_favorite" do
    assert_difference('MarketFavorite.count', -1) do
      delete market_favorite_url(@market_favorite)
    end

    assert_redirected_to market_favorites_url
  end
end
