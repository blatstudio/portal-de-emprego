require 'test_helper'

class SiteAssetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @site_asset = site_assets(:one)
  end

  test "should get index" do
    get site_assets_url
    assert_response :success
  end

  test "should get new" do
    get new_site_asset_url
    assert_response :success
  end

  test "should create site_asset" do
    assert_difference('SiteAsset.count') do
      post site_assets_url, params: { site_asset: { name: @site_asset.name } }
    end

    assert_redirected_to site_asset_url(SiteAsset.last)
  end

  test "should show site_asset" do
    get site_asset_url(@site_asset)
    assert_response :success
  end

  test "should get edit" do
    get edit_site_asset_url(@site_asset)
    assert_response :success
  end

  test "should update site_asset" do
    patch site_asset_url(@site_asset), params: { site_asset: { name: @site_asset.name } }
    assert_redirected_to site_asset_url(@site_asset)
  end

  test "should destroy site_asset" do
    assert_difference('SiteAsset.count', -1) do
      delete site_asset_url(@site_asset)
    end

    assert_redirected_to site_assets_url
  end
end
