# 1.1.9
 - Company views log

# 1.1.8
 - Companies can register users by their QR Code

# 1.1.7
 - Market banner

# 1.1.6
 - Mobile optimizations to job and market views

# 1.1.5
 - Fix company application when logged out

# # 1.1.4
 - Market page added
 - Users can apply to companies in the market, and undo the same action
 - Users can apply to companies using the company QR Code

# 1.1.3
 - Fix: Json format for companies to reflect the new names

# 1.1.2
 - Job Offers have their application number visible
 
# 1.1.1
 - Companies have field for website

# 1.1.0
 - Fixed company job offer link
 - Job offer link has changed

# 1.0.3
 - Index page now show job offers
 - Fix: Trashed job offers where visible to regular users
 - JobOffer manage pages are now inside the 'admin' scope

# 1.0.2
 - Fix: Unable to create markets

# 1.0.1
 - Company address changed to country
 - Company activity changed to description
 - Fix: company routes to allow edit and destroy in the admin panel, and view outside the admin scope
