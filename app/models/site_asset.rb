class SiteAsset < ApplicationRecord
  has_attached_file :attachment
  validates_attachment :attachment, :presence => true
  validates_attachment_content_type :attachment, :content_type => [/\Aimage\/.*\Z/, /\Avideo\/.*\Z/, /\Aaudio\/.*\Z/, /\Aapplication\/.*\Z/, /\Atext\/.*\Z/]
end
