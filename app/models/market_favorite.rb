class MarketFavorite < ApplicationRecord
  belongs_to :market
  belongs_to :company
  belongs_to :profile

  validates :market, :company, :profile, presence: true
  validate :cannot_be_duplicate

  def cannot_be_duplicate
    if MarketFavorite.find_by(market_id: self.market_id, company_id: self.company_id, profile_id: self.profile_id)
        errors.add(:duplicate, 'Informações já enviadas')
    end
  end

end
