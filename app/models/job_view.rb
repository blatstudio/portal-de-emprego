class JobView < ApplicationRecord
  belongs_to :user
  belongs_to :job_offer

  validates_presence_of :user, :job_offer

  validates :job_offer, uniqueness: { scope: :user }

  after_create :update_number_of_views
  after_destroy :update_number_of_views

  accepts_nested_attributes_for :job_offer

  def self.create_later user, job_offer
    JobViewCreateJob.perform_later user, job_offer
  end

  def update_number_of_views
    self.job_offer.update_view_count
  end
end
