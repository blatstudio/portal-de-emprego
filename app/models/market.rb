class Market < ApplicationRecord
    has_and_belongs_to_many :companies
    has_many :market_favorites, dependent: :destroy
    has_attached_file :banner, default_url: "/images/missing.png"
    validates_attachment_content_type :banner, content_type: /\Aimage\/.*\z/
end
