class JobOffer < ApplicationRecord
  include ActiveModel::Dirty

  has_many :applications, dependent: :destroy
  has_many :views, foreign_key: :job_offer_id, class_name: "JobView", dependent: :destroy
  has_and_belongs_to_many :programmes

  has_attached_file :image, styles: { medium: "300x300>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validate :expires_at_validation
  validates_presence_of :company,
    :position,
    :location,
    :expires_at,
    :type_of_contract,
    :contract_duration,
    :description,
    :name,
    :email,
    :phone

  has_many :users, through: :applications

  before_create :generate_random_id

  after_create :mail_new_job_offer

  before_validation on: [:create] do
    self.expires_at ||= Date.today
  end

  after_save :mail_approved

  def send_applications
    self.applications_sent = true
    if self.save
      JobOfferMailer.job_offer_expired(self).deliver_later
      return true
    end
    return false
  end

  def restore
    self.trashed = false
    self.save
  end

  def trash
    self.trashed = true
    if self.save
      unless self.expired?
        JobOfferMailer.job_offer_reproved(self).deliver_later
      end
      return true
    end
    return false
  end

  def approve
    self.approved = true
    if self.save
      JobOfferMailer.job_offer_approved(self).deliver_later
    end
  end

  def mail_approved
    if saved_change_to_approved? and approved == true
      JobOfferMailer.job_offer_approved(self).deliver_later
    end
  end

  def mail_new_job_offer
    JobOfferMailer.new_job_offer(self).deliver_later
  end

  def mail_link
    JobOfferMailer.applications_link(self).deliver_later
  end

  def expired?
    Date.today > expires_at
  end

  def expires_at_validation
    if expires_at > JobOffer.max_expiration_date
      errors.add(:expires_at, "máximo de 3 meses")
    end
  end

  def generate_random_id
    while(!JobOffer.find_by(random_id: (self.random_id = SecureRandom.hex)).blank?)
    end
  end

  def self.max_expiration_date
    Date.today + 3.months
  end

  def self.locations
    [
      'Aveiro',
      'Beja',
      'Braga',
      'Bragança',
      'Castelo Branco',
      'Coimbra',
      'Évora',
      'Faro',
      'Guarda',
      'Leiria',
      'Lisboa',
      'Portalegre',
      'Porto',
      'Santarém',
      'Setúbal',
      'Viana do Castelo',
      'Vila Real',
      'Viseu',
      'Madeira',
      'Açores'
    ]
  end

  def self.types_of_contract
    ['Efetivo', 'Estágio', 'Part-time', 'Outro']
  end

  def update_view_count
    self.view_count = self.views.count
    self.save
  end

  def update_applications_number
    self.number_of_applications = self.applications.count
    self.save
  end
end
