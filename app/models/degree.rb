class Degree < ApplicationRecord
  has_many :programmes, dependent: :destroy

  validates_presence_of :name

  before_save :set_order

  def set_order
    if self.order.blank?
      self.order = 1
    end
  end

end
