class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :confirmable, :recoverable, :rememberable, :trackable,
         :validatable, :invitable

  before_create :generate_random_string
  after_create :assign_default_role

  has_one :profile, dependent: :destroy
  has_many :applications, dependent: :destroy
  has_many :views, foreign_key: :user_id, class_name: "JobView", dependent: :destroy

  has_many :market_favorites, through: :profile

  has_and_belongs_to_many :companies, limit: 1

  accepts_nested_attributes_for :applications

  def generate_random_string
    while(!User.find_by(random_string: (self.random_string = SecureRandom.hex(3))).blank?)
    end
  end

  def self.generate_random_strings
    User.all.each do |user|
      user.generate_random_string
      user.save
    end
  end

  def self.filter query, filter
    case filter
    when 'approved'
      return query.where("profiles.complete": true, "profiles.reviewed": true)
    when 'approve'
      return query.where("profiles.complete": true, "profiles.reviewed": false)
    when 'incomplete'
      return query.where("profiles.complete": false)
    else
      return query
    end
  end

  def assign_default_role
    self.add_role :newuser if self.roles.blank?
  end

  def assign_role new_role
    self.remove_role :newuser
    self.add_role new_role
  end

  def complete?
    if self.has_role? :student
      return self.profile.complete?
    else
      return true
    end
  end

  def incomplete?
    !self.complete?
  end

  def make_student
    self.assign_role :student
  end

  def name
    if self.profile.blank?
      return self.email
    else
      return self.profile.name
    end
  end

  def create_view job_offer
    if self.has_role? :student
      JobView.create_later(self, job_offer)
    end
  end

  def profile_qr
    require 'rqrcode'
    RQRCode::QRCode.new( Rails.application.routes.url_helpers.user_url(self) )
  end

  def to_param
    self.random_string
  end

end
