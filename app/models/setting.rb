class Setting < ApplicationRecord
  validates :name, uniqueness: true
end
