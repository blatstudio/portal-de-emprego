class Company < ApplicationRecord
    has_and_belongs_to_many :users
    has_and_belongs_to_many :markets
    has_and_belongs_to_many :profiles # this is placeholder for student view of companies
    has_many :market_favorites, dependent: :destroy

    has_attached_file :logo, styles: { medium: "175x175>", thumb: "100x100>" }, default_url: Setting.find_by(name: 'Logo Top').value
    validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/
    validates :nif, uniqueness: true, allow_nil: true

    before_save :fix_website

    def profile_qr market_id = nil
        require 'rqrcode'
        if market_id
            path = Rails.application.routes.url_helpers.company_market_url(market_id, self, apply: true) 
        else
            path = Rails.application.routes.url_helpers.market_company_url(self, apply: true) 
        end
        RQRCode::QRCode.new path
    end

    def nif_check attribute
        if attribute === :name
            json_attribute = 'title'
        else
            json_attribute = attribute.to_s
        end
        self[attribute] === self.nif_json[json_attribute]
    end

    def nif_update
        if self.nif != nil && Company.can_search_nif?
            url = "http://www.nif.pt/?json=1&q=#{self.nif}&key=#{ENV['NIF_KEY']}"
            json = JSON.parse(Net::HTTP.get(URI.parse(url)))
            puts json
            if json['result'] === "success"
                company_json = json["records"][nif.to_s]
                self.name = company_json["title"] if self.name.blank?
                #self.address = company_json["address"] if self.address.blank?
                self.city = company_json["city"] if self.city.blank?
                self.description = company_json["activity"] if self.description.blank?
                self.nif_json = company_json
                self.nif_checked_at = DateTime.now
            end
            self.save
        end
    end

    def self.find_and_update_missing_info
        c = Company.where(nif_checked_at: nil).where.not(nif: nil).first
        c.nif_update unless c.blank?
    end

    def self.can_search_nif?
        url = "http://www.nif.pt/?json=1&credits=1&key=#{ENV['NIF_KEY']}"
        credits = JSON.parse(Net::HTTP.get(URI.parse(url)))['credits']
        puts credits
        credits['minute'] > 0
    end

    def self.new_by_nif nif
        company = Company.new
        if Company.can_search_nif?
            url = "http://www.nif.pt/?json=1&q=#{nif}&key=#{ENV['NIF_KEY']}"
            json = JSON.parse(Net::HTTP.get(URI.parse(url)))
            puts json
            if json['result'] === "success"
                company_json = json["records"][nif.to_s]
                company.name = company_json["title"]
                #company.address = company_json["address"]
                company.city = company_json["city"]
                company.description = company_json["activity"]
            end
        end
        company
    end

    def fix_website
        if !self.website.blank? and !self.website.start_with?('http')
            self.website = 'http://' + self.website
        end
    end

    def create_user
        unless self.email.blank?
            user = self.users.new email: self.email
            user.add_role :recruiter
            user.password = "jobshop@2018"
            user.skip_confirmation!
            user.save
            user.companies << self
        end
        user
    end
end
