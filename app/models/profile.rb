class Profile < ApplicationRecord
  include ActiveModel::Dirty

  belongs_to :user
  belongs_to :programme
  has_many :market_favorites, dependent: :destroy
  has_and_belongs_to_many :companies # this is placeholder for student view of companies
  
  validates_presence_of :name, :user

  after_create :make_student
  before_save :needs_review

  has_attached_file :curriculum
  validates_attachment_content_type :curriculum, content_type: "application/pdf"

  def email
    self.user.email
  end

  def degree
    self.programme.degree.name
  end

  def incomplete?
    !self.complete?
  end

  private

  def make_student
    self.user.make_student
  end

  def needs_review
    unless reviewed_changed? and curriculum.present?
      self.reviewed = false
    end
    unless complete_changed?
      if curriculum.present?
        self.complete = true
      else
        self.complete = false
      end
    end
  end

end
