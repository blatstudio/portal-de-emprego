class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.has_role? :blat
      can :manage, :all

    elsif user.has_role? :admin
      can :manage, :all
      cannot :manage, Market
      cannot :manage, Setting
      can :manage, Company
      can [:read, :update], Market.where('active', true)

    elsif user.has_role? :recruiter
      can :read, user.companies.first # Can view own company
      can :read, User # Can view users
      can :read, Market.where('active', true) # Can view active markets

    elsif user.has_role? :student
      can :manage, user
      can :manage, user.profile
      can :read, JobOffer
      can :show, Company
      can :read, Market.where('active', true)

    elsif user.has_role? :newuser
      can :create, Profile
    end

    can :create, JobOffer
    can :read, Degree
    can :read, Programme

    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
