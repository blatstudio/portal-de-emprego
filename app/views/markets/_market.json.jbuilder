json.extract! market, :id, :name, :active, :created_at, :updated_at
json.url market_url(market, format: :json)
