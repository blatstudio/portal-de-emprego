json.extract! job_offer, :id, :company, :end_time, :position, :created_at, :updated_at, :location, :type_of_contract, :contract_duration
json.minimum_requirements simple_format(job_offer.minimum_requirements)
json.description simple_format(job_offer.description)
json.expires_at l job_offer.expires_at
json.requirements job_offer.programmes
json.url job_offer_url(job_offer, format: :json)
json.start_time job_offer.start_time.strftime("%H:%M")
json.end_time job_offer.end_time.strftime("%H:%M")
json.salary number_to_currency job_offer.salary
json.image job_offer.image.url
json.applied job_offer.users.exists? current_user.id
if job_offer.users.exists?(current_user.id)
  json.application_url application_url(job_offer.applications.find_by user: current_user)
end
