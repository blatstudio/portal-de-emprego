json.extract! site_asset, :id, :name, :created_at, :updated_at
json.url site_asset_url(site_asset, format: :json)
