json.extract! programme, :id, :name, :degree_id, :created_at, :updated_at, :full_name
json.url programme_url(programme, format: :json)
