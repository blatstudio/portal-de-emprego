json.extract! market_favorite, :id, :market_id, :company_id, :profile_id, :created_at, :updated_at
json.url market_favorite_url(market_favorite, format: :json)
