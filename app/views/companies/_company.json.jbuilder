json.extract! company, :id, :name, :country, :city, :description, :website, :email, :created_at, :updated_at
json.logo company.logo.url
json.description simple_format(company.description)
json.favorite mh_is_company_favorite?(@market, company)
json.favorite_url mh_is_company_favorite?(@market, company) ? market_favorite_url(mh_company_favorite(@market, company)) : nil
json.edit_url edit_company_url(company)
json.url company_url(company, format: :json)
