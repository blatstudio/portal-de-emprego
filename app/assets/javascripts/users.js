document.addEventListener('DOMContentLoaded', () => {
  Dropzone.autoDiscover = false;
  var link = $('#curriculum-url');
  var button = $('#curriculum-upload-button');
  var destroyButton = $('.curriculum-destroy');
  var text = "";

  $("#curriculum-upload-form").dropzone({
    paramName: "profile[curriculum]",
    clickable: "#curriculum-upload-button",
    parallelUploads: 1,
    acceptedFiles: 'application/pdf',
    maxFilesize: 5,
    previewTemplate: "<div></div>",
    dictFileTooBig: "Ficheiro demasiado grande ({{filesize}}MiB). Máximo permitido: {{maxFilesize}}MiB",
    dictInvalidFileType: "Só pode enviar ficheiros do tipo PDF",
    init: function() {

      text = button.html();
      var destroyButtonClass = destroyButton.attr('class');

      function resetForm() {
        button.removeClass('yellow');
        button.removeClass('orange');
        button.html(text);
        destroyButton.attr('class', destroyButtonClass);
        console.log(destroyButtonClass);
      }

      // ERROR
      this.on('error', function(file, errorMessage) {
        console.log('error');
        console.log(errorMessage);
        resetForm();
        button.html(button.html() + "<strong>" + errorMessage + "</strong>");
      });

      // PROCESSING
      this.on('processing', function(file) {
        console.log('processing');
        button.addClass('orange');
        button.html('A enviar: 0%');
        destroyButton.addClass('hidden');
      });

      // PROGRESS
      this.on('uploadprogress', function(file, progress, bytesSent) {
        console.log('progress');
        button.html('A enviar: ' + Math.round(progress) + '%');
        if(progress == 100) {
          button.removeClass('orange');
          button.addClass('yellow');
          button.html('A processar');
        }
      });

      // SUCCESS
      this.on('success', function(file, response) {
        link.attr('href', response.curriculum_url);
        link.removeClass('missing');
        resetForm();
        destroyButton.removeClass('hidden');
      });
    }
  });
});
