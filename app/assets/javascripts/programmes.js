document.addEventListener('DOMContentLoaded', () => {
  var $menu = $('#adminmenu .menu>li');
  var $submenus = $menu.find(".submenu");

  $submenus.css('pointer-events', 'none');

  $submenus.on('transitionend', function() {
    var $this = $(this);
    if($this.is('.active'))
      $this.css('pointer-events', '');
    else
      $this.css('pointer-events', 'none');
  });

  function toggle($this) {
    console.log('togle');
    var $submenu = $this.find('.submenu');
    if( $submenu.is('.active') ) {
      close($this);
    } else {
      open($this);
    }
  }

  function open($this) {
    console.log('open');
    var $submenu = $this.find('.submenu');
    $menu.find('.submenu').removeClass('active');
    $submenu.addClass('active');
  }

  function close($this) {
    var $submenu = $this.find('.submenu:not(:hover)');
    $submenu.removeClass('active');
  }

  $menu.on('mouseenter', function(e) {
    console.log('hover in');
    var $this = $(this);
    open($this);
  });
  $menu.on('mouseleave', function(e) {
    console.log('hover out');
    var $this = $(this);
    $this.menu = setTimeout(function() {
      close($this);
    }, 250);
  });

  $menu.on('touchend', function(e) {
    console.log('click');
    var $this = $(this);
    toggle($this);
  });

  $('.menu>li>a[href="#"], .menu>li>a[href=""]').click(function(e) {
    e.preventDefault()
  });

})
