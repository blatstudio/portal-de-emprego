document.addEventListener('DOMContentLoaded', () => {
    var $marketForm = $("#market_form");
    if( $marketForm.length > 0 )  {
        var $marketCompanyIds = $("#market_company_ids");
        $marketCompanyIds.attr('name', $marketCompanyIds.attr('name') + '[]');
        $marketCompanyIds.attr('id', '');

        var $companiesList = $("#market_companies_list");
        var $companiesSelect = $("#market_companies_select > select");
        var $addButton = $("#market_companies_select > a");

        var marketCompanyIds = $marketCompanyIds.val().split(" ");
        for(var i = 0; i < marketCompanyIds.length; i++) {
            var id = marketCompanyIds[i];
            if( id !== "" ) {
                addCompany(id);
            }
        }
        $marketCompanyIds.val('');

        $addButton.click(function(e) {
            e.preventDefault();
            addCompany($companiesSelect.val());
        })

        function addCompany(id) {
            if( id !== '' ) {
                $companyOption = $companiesSelect.find('option[value="' + id + '"]');
                $companyOption.css('display', 'none');
                $companiesSelect.val('');

                var li = $('<li>');
                li.data('id', id);
                li.attr('data-id', id);
                if(marketCompanyIds.includes(id)) {
                    li.html($companyOption.html());
                } else {
                    li.append($('<strong>').html($companyOption.html()));
                }
                li.appendTo($companiesList);

                $companyId = $marketCompanyIds.clone();
                $companyId.data('id', id);
                $companyId.val(id);
                $marketForm.append($companyId);
            }
        }

        $companiesList.click('li', function(e) {
            var $target = $(e.target);
            var id = $target.data('id');
            var $input = $marketForm.find('input[value="' + id + '"]');
            $target.remove();
            $input.remove();

            $companyOption = $companiesSelect.find('option[value="' + id + '"]');
            $companyOption.css('display', '');
        });
    }

});
