document.addEventListener('DOMContentLoaded', () => {
  $('table tbody tr').each(function() {
    jthis = $(this);
    var a = jthis.find('a');
    var href = a.attr('href');

    jthis.click(function(e) {
      var tagName = e.target.tagName.toUpperCase();
      if(tagName != 'A' && tagName != 'I') {
        Turbolinks.visit(href);
      }
    });
  });
});
