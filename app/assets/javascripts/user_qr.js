document.addEventListener('DOMContentLoaded', () => {
  $profile = $('#user, #company, #user-profile');

  if( $profile.length > 0 ) {
    var $svg = $profile.find('svg');

    $svg.each( function() {
      var $this = $(this);
      this.setAttribute('viewBox', "0 0 " + $this.attr('width') + " " + $this.attr('height') )
    })
  }
})
