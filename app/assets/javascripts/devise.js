document.addEventListener('DOMContentLoaded', () => {
  var $devise = $("#devise");
  var focusElements = 'input, select';

  setFocus($devise.find(":focus"));

  $devise.on('focus', focusElements, function(e) {
    setFocus($(e.target));
  });

  $devise.on('blur', focusElements, function(e) {
    removeFocus($(e.target));
  });

  function setFocus($element) {
    getParent($element).addClass("focus");
  };

  function removeFocus($element) {
    getParent($element).removeClass("focus");
  };

  function getParent($element) {
    var $parent = $element.parent();
    while(!$parent.hasClass('field') && $parent.length > 0) {
      $parent = $parent.parent();
    }
    return $parent;
  }

  // Require terms acceptance
  var $acceptance = $devise.find('.accept input');
  if( $acceptance.length > 0 ) {
    $devise.find('form').submit(function(e) {
      if( $acceptance.is(':checked') ) {
        console.log('checked')
      } else {
        e.preventDefault();
      }
    });
  }
});
