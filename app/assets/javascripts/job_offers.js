document.addEventListener('DOMContentLoaded', () => {
  var $panel = $('.job-offer-panel');
  var closeButton = $('#job-offer-close');

  $('a.job-link').each(function() {
    var $img = $(this).find('.job-image img');
    var $clone = $img.clone();
    $clone.addClass('bg');
    $clone.prependTo($img.parent());
  });

  $('a.job-link').click(function(e) {
    e.preventDefault();
    var url = this.href;
    var id = $(this).data('id');

    $.getJSON(url, function(job) {
      $('#job-company').html(job.company);
      $('#job-position').html(job.position);
      $('#job-location').html(job.location);
      $('#job-expires-at').html(job.expires_at);
      var job_requirements = $('#job-requirements');
      job_requirements.empty();
      for(var programme in job.requirements) {
        job_requirements.html(job_requirements.html() + job.requirements[programme].name + '</br>');
      }
      $('#job-minimum-requirements').html(job.minimum_requirements);
      $('#job-contract').html(job.type_of_contract);
      $('#job-duration').html(job.contract_duration);
      $('#job-start').html(job.start_time);
      $('#job-end').html(job.end_time);
      $('#job-description').html(job.description);
      $('#job-salary').html(job.salary);
      $('#application_job_offer_id').val(job.id);
      $('#job-img').attr('src', job[ $('#job-img').data('src') ] );

      $panel.find('input.get-info').each(function() {
        var $this = $(this);
        var info = job[ $this.data('src') ];
        $this.val(info);
      });
      $panel.find('.get-info:not(input)').each(function() {
        var $this = $(this);
        if(!$this.data('text')) {
          var info = job[ $this.data('src') ];
          if ( info === null  || info === '' ) {
            $this.parent().hide();
          } else {
            $this.parent().show();
            $this.html( job[ $this.data('src') ] );
          }
        }
      });
      $panel.find('a.get-info').each(function() {
        var info = job[ $(this).data('src') ];
        $(this).attr('href', info );
      });

      $applicationLink = $('#job-offer-link');
      $newApplicationButton = $panel.find('form .button');
      if( job.applied || job.favorite ) {
        $newApplicationButton.addClass('hide');
        $applicationLink.removeClass('hide');
        $applicationTrueLink = $('#application' + id);
        $applicationLink.click(function(e) {
          var $this = $(this);
          console.log($this.attr('href'));
          e.preventDefault();
          $applicationTrueLink.click();
        })
      } else {
        $newApplicationButton.removeClass('hide');
        $applicationLink.addClass('hide');
      }
      $('body').css('overflow', 'hidden');
      openPanel();
    });


    $panel.click(closePanel);
    closeButton.click(closePanel);

    $panel.children().click(function(e) {
      e.stopPropagation();
    });

  });

  function closePanel() {
    $panel.removeClass('active');
    $panel.one('transitionend', function() {
      $panel.addClass('hide');
    });
    $('body').css('overflow', '');
  }

  function openPanel() {
    $panel.removeClass('hide');
    setTimeout(function() {
      $panel.addClass('active');
    }, 20);
  }

});
