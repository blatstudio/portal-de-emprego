class API {
  constructor() {
  }

  fetch = {
    contractTypes() { return axios.get('/api/contract_types') },
    degrees() { return axios.get('/api/degrees') },
    locations() { return axios.get('/api/locations') },
    programmes(degree = null) { return degree == null ? axios.get(`/api/programmes`) : axios.get(`/api/degrees/${degree.id}/programmes`) }
  }
}
window.api = new API();
