require('./load_axios.js');
require('./api.js');

window.moment = require('moment');

import Vue from 'vue';
window.Vue = Vue;

let app = null;
import App from './components/app';
document.addEventListener('DOMContentLoaded', () => {
  $(document).foundation();
  if(document.querySelector("#job-offer") != null) {
    app = new Vue({
      el: '#job-offer',
      // router,
      // store: vuexStore,
      data: {
        // api: api,
      },
      computed: {
        csrf() {
          return document.head.querySelector('meta[name="csrf-token"]').content;
        }
      },
      render: h => h(App),
      mounted() {
      }
    });
  }
})
