module PagesHelper
  def search_params(args = {})
    search_params = params.permit('programme', 'location', 'contract', 'order', 'filter')
    if args[:programme]
      search_params[:programme] = args[:programme].id
    end
    if args[:location]
      search_params[:location] = args[:location]
    end
    if args[:contract]
      search_params[:contract] = args[:contract]
    end
    if args[:order] 
      if search_params[:order].to_s.eql? args[:order].to_s
        search_params = search_params.except(:order)
      else
        search_params[:order] = args[:order]
      end
    end
    if args[:filter]
      if search_params[:filter].to_s.eql? args[:filter].to_s
        search_params = search_params.except(:filter)
      else
        search_params[:filter] = args[:filter]
      end
    end
    search_params
  end

  def remove_search_param(args = [])
    params.permit(:programme, :location, :contract, :order, :filter).except(args)
  end
end
