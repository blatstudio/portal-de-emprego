module MarketsHelper
    def mh_is_company_favorite? market, company
        !mh_company_favorite( market, company ).blank?
    end

    def mh_company_favorite market, company
        if market.blank? or current_user.profile.blank?
            return nil
        end
        current_user.profile.market_favorites.find_by(market_id: market.id, company_id: company.id)
    end
end
