class BlatController < ApplicationController
  before_action :authenticate_user!, :authorized?
  layout 'admin'

  def index
  end

  private
  def authorized?
    authorize! :manage, :all
  end
end
