class PagesController < ApplicationController
  before_action :authenticate_user!, except: [:index]

  def index
    if user_signed_in?
      if current_user.has_role? :blat
        redirect_to blat_path
      elsif current_user.has_role? :admin
        redirect_to degrees_path
      elsif current_user.has_role? :recruiter
        redirect_to market_company_path(current_user.companies.first)
      elsif current_user.has_role? :newuser
        redirect_to new_profile_path
      else
        @filters = {}
        if params[:programme]
          @filters[:programme] = Programme.find(params[:programme]).full_name
          @job_offers = JobOffer.left_joins(:programmes).where(search_params).order(order_params)
        else
          @job_offers = JobOffer.where(search_params).order(order_params)
        end
        if params[:location]
          @filters[:location] = params[:location]
        end
        if params[:contract]
          @filters[:contract] = params[:contract]
        end
      end
    else
      redirect_to new_user_session_path
    end
  end

  def market
    if current_user.has_role? :recruiter
      redirect_to market_company_path(current_user.companies.first)
    else
      if( params[:id] )
        @market = Market.find params[:id]
      else
        @market = Market.find_by(active: true)
      end
      if params[:filter] === 'mine' && !current_user.profile.blank?
        @companies = @market.companies
          .joins(:market_favorites)
          .where(market_favorites: current_user.market_favorites)
          .order(market_order)
      else
        @companies = @market.companies.order(market_order)
      end
      render layout: 'markets'
    end
  end

  private
    def market_order
      order_param = params[:order]
      order = {}
      if order_param === 'views'
        order = {views: :desc}
      end
      order[:name] = :asc
      order
    end

    def search_params
      search_params = params.permit(:programme, :approved, :location, :contract, :filter)
      search = {expires_at: Date.today..(Date.today + 3.months), trashed: false}
      if search_params[:contract]
        search[:type_of_contract] = search_params[:contract]
      end
      if search_params[:programme]
        search['job_offers_programmes.programme_id'] = search_params[:programme]
      end
      if search_params[:location]
        search[:location] = search_params[:location]
      end
      if search_params[:filter] == 'applications'
        search[:number_of_applications] = 0
      elsif search_params[:filter] == 'expiring'
        search[:expires_at] = Date.today..(Date.today + 2.weeks)
      end
      search[:approved] = true
      search
    end

    def order_params
      order_param = params[:order]
      order = {}
      if order_param == 'applications'
        order = {number_of_applications: :desc}
      elsif order_param == 'views'
        order = {view_count: :desc}
      end
      if params[:filter] == 'expiring'
        order[:expires_at] = :asc
      end
      order[:expires_at] = :asc
      #order[:updated_at] = :desc
      order
    end
end
