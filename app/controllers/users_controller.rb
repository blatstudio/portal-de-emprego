class UsersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: [:show, :random_index, :qr_code]
  before_action :set_user, only: [:show, :update, :destroy, :qr_code]

  def qr_code
    data = @user.profile_qr.as_png(size: 512, border_modules: 4).to_s
    send_data data, type: 'image/png', filename: 'qr.png'
  end

  def random_index
    authorize! :manage, User
    @users = User.without_role(:blat).order(:random_string)
  end

  def index
    if current_user.has_role? :blat
      @role = 'all'
      @users = User.all.order(:email)
      render layout: 'admin'
    else
      params[:role] ||= 'student'
      @filter = params[:filter] ||= 'all'
      @role = params[:role]
      @order = params[:order] || "email"
      case @order
      when "email"
        @order = "users.email"
      when "name"
        @order = "profiles.name"
      when "reviewed"
        @order = "profiles.reviewed"
      else
        @order = ""
      end
      puts @order

      if @role == 'student'
        @users = User.with_role(@role).joins(:profile).order(@order, "profiles.name")
      else
        @users = User.with_role(@role).order(@order)
      end

      @users = User.filter(@users, @filter)

      render layout: 'admin'
    end
  end

  def show
    authorize! :show, @user

    if current_user.has_role? :recruiter
      @company = current_user.companies.first
      @market = Market.find_by(active: true)
      @favorite = @user.profile.market_favorites.new(market: @market, company: @company)
      @favorite.save
      notice = "Registadas as informações de #{@user.name}"
      redirect_to @company, notice: notice
    end

    unless @user.profile.blank?
      @profile = @user.profile
    end

    if can? :manage, User
      render :admin_show, layout: 'admin'
    end
  end

  def update
    if @user.update(user_params)
      redirect_to job_offers_path, notice: 'User atualizado'
    else
      redirect_to job_offers_path, notice: 'Erro a atualizar utilizador'
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'Utilizador "' + @user.name + '" eliminado' }
      format.json { head :no_content }
    end
  end

  private
    def user_params
      params.require(:user).permit(application: [:user_id, :job_offer_id])
    end

    def set_user
      if params[:id].blank?
        @user = current_user
      else
        @user = User.find_by random_string: params[:id]
      end
    end

end
