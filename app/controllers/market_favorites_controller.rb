class MarketFavoritesController < ApplicationController
  before_action :set_market_favorite, only: [:show, :edit, :update, :destroy]

  # GET /market_favorites
  # GET /market_favorites.json
  def index
    @market_favorites = MarketFavorite.all
  end

  # GET /market_favorites/1
  # GET /market_favorites/1.json
  def show
  end

  # GET /market_favorites/new
  def new
    @market_favorite = MarketFavorite.new
  end

  # GET /market_favorites/1/edit
  def edit
  end

  # POST /market_favorites
  # POST /market_favorites.json
  def create
    @profile = current_user.profile
    @market_favorite = @profile.market_favorites.new(market_favorite_params)

    respond_to do |format|
      if @market_favorite.save
        format.html { redirect_back fallback_location: @market_favorite.market, notice: 'Market favorite was successfully created.' }
        format.json { render :show, status: :created, location: @market_favorite }
      else
        format.html { render :new }
        format.json { render json: @market_favorite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /market_favorites/1
  # DELETE /market_favorites/1.json
  def destroy
    #@market_favorite = current_user.market_favorites.find_by(market_id: params[:market_id], company_id: params[:company_id])
    @market_favorite.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: @market_favorite.market, notice: 'Market favorite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_market_favorite
      @market_favorite = MarketFavorite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def market_favorite_params
      params.require(:market_favorite).permit(:market_id, :company_id, :profile_id)
    end
end
