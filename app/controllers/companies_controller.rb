class CompaniesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_company, only: [:show, :edit, :update, :destroy, :company_qr]
  layout 'admin', except: [:show]

  def company_qr
    data = @company.profile_qr.as_png(size: 512, border_modules: 4).to_s
    send_data data, type: 'image/png', filename: 'qr.png'
  end

  def qr_codes
    @companies = Company.all.order(:name)
  end

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all.order(:name)
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    if( params[:market_id] )
      @market = Market.find params[:market_id]
    else
      @market = @company.markets.find_by(active: true)
    end
    authorize! :read, @market
    if current_user.has_role? :recruiter and current_user.valid_password? 'jobshop@2018'
      redirect_to edit_registration_path(current_user)
    else

      unless current_user.profile.blank?
        unless current_user.profile.companies.exists? @company.id
          current_user.profile.companies << @company
          @company.views = @company.profiles.count
          @company.save
        end
      end

      if( params[:apply] === 'true' )
        @favorite = current_user.profile.market_favorites.new(market: @market, company: @company)
        begin
          @favorite.save
          flash.now[:notice] = 'Informações submetidas com sucesso!'
        rescue ActiveRecord::RecordNotUnique
          flash.now[:notice] = 'Já submeteste informações à ' + @company.name
        end
      end
      render layout: 'markets'
    end
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save
        format.html { redirect_to Company, notice: "Empresa '#{@company.name}' criada" }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to edit_company_path(@company), notice: "Empresa '#{@company.name}' atualizada" }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: "Empresa '#{@company.name}' eliminada" }
      format.json { head :no_content }
    end
  end

  def nif_lookup
    render json: Company.new_by_nif(params[:nif])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :country, :city, :description, :nif, :logo, :website, :email)
    end
end
