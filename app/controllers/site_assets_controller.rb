class SiteAssetsController < ApplicationController
  before_action :set_site_asset, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /site_assets
  # GET /site_assets.json
  def index
    @site_assets = SiteAsset.all
  end

  # GET /site_assets/1
  # GET /site_assets/1.json
  def show
  end

  # GET /site_assets/new
  def new
    @site_asset = SiteAsset.new
  end

  # GET /site_assets/1/edit
  def edit
  end

  # POST /site_assets
  # POST /site_assets.json
  def create
    @site_asset = SiteAsset.new(site_asset_params)

    respond_to do |format|
      if @site_asset.save
        format.html { redirect_to @site_asset, notice: 'Site asset was successfully created.' }
        format.json { render :show, status: :created, location: @site_asset }
      else
        format.html { render :new }
        format.json { render json: @site_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /site_assets/1
  # PATCH/PUT /site_assets/1.json
  def update
    respond_to do |format|
      if @site_asset.update(site_asset_params)
        format.html { redirect_to @site_asset, notice: 'Site asset was successfully updated.' }
        format.json { render :show, status: :ok, location: @site_asset }
      else
        format.html { render :edit }
        format.json { render json: @site_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /site_assets/1
  # DELETE /site_assets/1.json
  def destroy
    @site_asset.destroy
    respond_to do |format|
      format.html { redirect_to site_assets_url, notice: 'Site asset was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site_asset
      @site_asset = SiteAsset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_asset_params
      params.require(:site_asset).permit(:name, :attachment)
    end
end
