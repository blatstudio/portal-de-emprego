class ProfilesController < ApplicationController
  load_and_authorize_resource except: [:show, :edit]
  before_action :set_user
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  layout 'devise'

  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    authorize! :manage, @profile
    if can? :manage, Profile
      render :admin_show
    end
  end

  # GET /profiles/new
  def new
    unless @user.profile.blank?
      redirect_to edit_user_profile_path(id: @user.profile, user_id: @user)
    end
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
    authorize! :manage, @profile
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    if @user == current_user
      @profile.user = @user
    end

    respond_to do |format|
      if @profile.save
        if @profile.user == current_user
          format.html { redirect_to my_path, notice: 'O teu perfil foi criado com sucesso. Não te esqueças de fazer upload do teu curruculum.' }
        else
          format.html { redirect_to @user, notice: 'Profile was successfully created.' }
        end
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    if profile_params[:curriculum] == ''
      @profile.curriculum = nil
    end
    respond_to do |format|
      if @profile.update(profile_params)
        if @profile.user == current_user
          format.html { redirect_to my_path, notice: 'O teu perfil foi atualizado com sucesso.' }
        else
          format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        end
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # DELETE /delete_curriculum/:id
  def delete_curriculum
    @profile = Profile.find params[:id]
    @profile.curriculum = nil
    @profile.save
    redirect_to @profile.user
  end

  # PATCH /profile/:id/incomplete
  def incomplete_profile
    @profile = Profile.find params[:id]
    @profile.reviewed = true
    @profile.complete = false
    @profile.save
    redirect_to @profile.user
  end

  # PATCH /toggle_review/:id
  def toggle_review
    @profile = Profile.find params[:id]
    @profile.reviewed = !@profile.reviewed
    @profile.save
    redirect_to @profile.user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = @user.profile
      @profile ||= Profile.find params[:id]
      @user = @profile.user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:name, :curriculum, :phone, :programme_id)
    end

    def set_user
      @user = current_user
    end
end
