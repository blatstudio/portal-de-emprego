class JobOfferMailer < ApplicationMailer

  def new_job_offer(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: "Proposta submetida"
  end

  def job_offer_expired(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: "Proposta expirou"
  end

  def job_offer_approved(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: "Proposta aprovada"
  end

  def job_offer_reproved(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: "Proposta reprovada"
  end

  def applications_link(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: "Candidaturas"
  end

end
