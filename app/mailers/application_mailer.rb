class ApplicationMailer < ActionMailer::Base
  default from: "\"#{ENV['APP_NAME']}\" <#{ENV['MAIL_SENDER']}>"
  layout 'mailer'
end
