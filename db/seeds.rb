# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


admin = User.create({email: 'contact@blatstudio.com', password: 'poqwpoqw', confirmed_at: DateTime.now})
admin.assign_role :blat


admin = User.create({email: 'admin@admin.com', password: 'adminpassword', confirmed_at: DateTime.now})
admin.assign_role :admin

# Settings
Setting.create({name: 'Logo', value: '/logo.png'})
Setting.create({name: 'Logo Top', value: '/logo.png'})
Setting.create({name: 'Nome', value: 'Portal de Emprego'})
