class ChangeActivityToDescriptionInCompany < ActiveRecord::Migration[5.2]
  def change
    rename_column :companies, :activity, :description
  end
end
