class AddContactInformationToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :email, :string
    add_column :job_offers, :name, :string
    add_column :job_offers, :phone, :string
  end
end
