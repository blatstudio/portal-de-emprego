class AddAttachmentImageToJobOffers < ActiveRecord::Migration
  def self.up
    change_table :job_offers do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :job_offers, :image
  end
end
