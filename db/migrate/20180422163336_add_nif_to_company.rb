class AddNifToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :nif, :integer
  end
end
