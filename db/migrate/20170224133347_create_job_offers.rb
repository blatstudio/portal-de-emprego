class CreateJobOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :job_offers do |t|
      t.string :company
      t.time :start_time
      t.time :end_time
      t.text :description
      t.string :position
      t.decimal :salary, precision: 8, scale: 2

      t.timestamps
    end
  end
end
