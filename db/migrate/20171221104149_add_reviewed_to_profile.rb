class AddReviewedToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :reviewed, :boolean, default: false
  end
end
