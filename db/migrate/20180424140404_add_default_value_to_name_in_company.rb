class AddDefaultValueToNameInCompany < ActiveRecord::Migration[5.2]
  def up
    change_column :companies, :name, :string, default: ''
  end

  def down
    change_column :companies, :name, :string, default: nil
  end
end
