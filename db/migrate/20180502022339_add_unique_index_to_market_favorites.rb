class AddUniqueIndexToMarketFavorites < ActiveRecord::Migration[5.2]
  def change
    add_index :market_favorites, [:market_id, :company_id, :profile_id], unique: true, name: 'unique_market_company_profile'
  end
end
