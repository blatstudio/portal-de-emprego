class CreateMarketFavorites < ActiveRecord::Migration[5.2]
  def change
    create_table :market_favorites do |t|
      t.references :market, foreign_key: true
      t.references :company, foreign_key: true
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
