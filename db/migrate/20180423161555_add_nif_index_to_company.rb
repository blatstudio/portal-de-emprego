class AddNifIndexToCompany < ActiveRecord::Migration[5.2]
  def change
    add_index :companies, :nif, unique: true
  end
end
