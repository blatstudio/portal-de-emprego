class AddAttachmentCurriculumToProfiles < ActiveRecord::Migration
  def self.up
    change_table :profiles do |t|
      t.attachment :curriculum
    end
  end

  def self.down
    remove_attachment :profiles, :curriculum
  end
end
