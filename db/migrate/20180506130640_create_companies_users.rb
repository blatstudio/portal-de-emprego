class CreateCompaniesUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :companies_users, id: false do |t|
      t.references :company, foreign_key: true, index: true
      t.references :user, foreign_key: true, index: true
    end
  end
end
