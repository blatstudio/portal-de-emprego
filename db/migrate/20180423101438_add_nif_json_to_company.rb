class AddNifJsonToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :nif_json, :json
  end
end
