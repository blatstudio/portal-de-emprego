class AddNifCheckedAtToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :nif_checked_at, :datetime
  end
end
