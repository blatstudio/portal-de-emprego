class CreateProfileCompanyViewsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :companies, :profiles do |t|
      # t.index [:company_id, :profile_id]
      t.index [:profile_id, :company_id], unique: true
    end
  end
end
