class AddRandomIdToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :random_id, :string
  end
end
