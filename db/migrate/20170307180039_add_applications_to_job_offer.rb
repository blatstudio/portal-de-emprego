class AddApplicationsToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :number_of_applications, :integer, default: 0
  end
end
