class AddAttachmentBannerToMarkets < ActiveRecord::Migration[5.2]
  def self.up
    change_table :markets do |t|
      t.attachment :banner
    end
  end

  def self.down
    remove_attachment :markets, :banner
  end
end
