class ChangeCompanyActivityToText < ActiveRecord::Migration[5.2]
  def up
    change_column :companies, :activity, :text
  end

  def down
    change_column :companies, :activity, :string
  end
end
