class AddTrashedToJobOffer < ActiveRecord::Migration[5.1]
  def change
    add_column :job_offers, :trashed, :boolean, default: false
  end
end
