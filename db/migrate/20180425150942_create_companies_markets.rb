class CreateCompaniesMarkets < ActiveRecord::Migration[5.2]
  def change
    create_table :companies_markets, id: false do |t|
      t.references :company, foreign_key: true, index: true
      t.references :market, foreign_key: true, index: true
    end
  end
end
