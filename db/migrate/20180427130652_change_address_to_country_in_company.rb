class ChangeAddressToCountryInCompany < ActiveRecord::Migration[5.2]
  def change
    rename_column :companies, :address, :country
  end
end
