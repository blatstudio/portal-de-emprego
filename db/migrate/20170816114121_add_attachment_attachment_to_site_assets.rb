class AddAttachmentAttachmentToSiteAssets < ActiveRecord::Migration[5.1]
  def self.up
    change_table :site_assets do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :site_assets, :attachment
  end
end
