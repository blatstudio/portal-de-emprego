class AddContractFieldsToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :type_of_contract, :string
    add_column :job_offers, :contract_duration, :string
  end
end
