class CreateSiteAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :site_assets do |t|
      t.string :name

      t.timestamps
    end
  end
end
