class AddCompleteToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :complete, :boolean, default: false
  end
end
